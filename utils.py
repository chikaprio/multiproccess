import random
import string


def random_string(length=10):
    letters = string.ascii_lowercase
    message = ''.join(random.choice(letters) for i in range(length))
    return message
