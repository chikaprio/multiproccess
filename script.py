import time
from multiprocessing import Queue, Process
from utils import random_string

_queue = Queue()


class Message:
    data = None

    def set_data(self, data):
        self.data = data

    def get_data(self):
        return self.data


class RandomMessageFactory:

    def create_message(self):
        message = Message()
        message.set_data(random_string())
        return message


def add_to_queue(message):
    _queue.put(message)


def execute_message(queue):
    while True:
        message = queue.get()
        print(message.get_data())


def main():
    processes = []
    for _ in range(10):
        process = Process(target=execute_message, args=(_queue,))
        processes.append(process)
        process.daemon = True
        process.start()

    message_factory = RandomMessageFactory()
    for _ in range(10000):
        message = message_factory.create_message()
        add_to_queue(message)

    for process in processes:
        process.join()


if __name__ == '__main__':
    main()
